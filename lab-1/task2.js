"use strict";

var readline = require('readline-sync');

var storage = []

function showMenu() {
    console.log("Хранилище детей.\n\
1 - информация о студенте\n\
2 - добавить студента\n\
3 - удалить студента\n\
4 - изменить параметры студента\n\
5 - показать среднюю оценку студента\n\
6 - показать информацию о студентах в заданной группе\n\
7 - показать студента, имеющего наибольшее количество оценок в заданной группе\n\
8 - показать студентов, у которых не оценок\n\
9 - показать меню\n\
10 - выйти\
    ");
}

function addStudent(group, id, marks) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["id"] == id) {
            console.log("Студент с такой зачеткой уже существует!");
            return;
        } 
    }

    let student = {};
    student["group"] = group;
    student["id"] = id;
    student["marks"] = marks
    storage.push(student);
}

function showStudent(id) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["id"] == id) {
            console.log(storage[i]["group"] + " " + id + "\n" + "Оценки:");
            for (let j = 0; j < storage[i]["marks"].length; j++) {
                console.log(storage[i]["marks"][j]);
            }
            return;
        } 
    }

    console.log("Студента с такой зачеткой нет!");
}

function removeStudent(id) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["id"] == id) {
            storage.splice(i, 1);
            return;
        }
    }

    console.log("Студента с такой зачеткой нет!");
}

function updateStudent(group, id, marks) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["id"] == id) {
            storage[i]["group"] = group;
            storage[i]["marks"] = marks;
            return;
        }
    }

    console.log("Студента с такой зачеткой нет!");
}

function getMidMark(id) {
    let sum = 0;

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["id"] == id) {
            for (let j = 0; j < storage[i]["marks"].length; j++) {
                sum += storage[i]["marks"][j];
            }

            return sum / storage[i]["marks"].length;
        }
    }  

    return null;
}

function studentsByGroup(group) {
    let result = []
    
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["group"] == group) {
            result.push(storage[i]);
        }
    } 

    return result;
}

function studentWithMaxMarks(group) {
    let student;
    let maxMarks = 0;

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["group"] == group && storage[i]["marks"].length >= maxMarks) {
            maxMarks = storage[i]["marks"].length;
            student = storage[i];
        }
    }  

    return student;
}

function studentsNoMarks() {
    let students = [];

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["marks"].length == 0) {
            students.push(storage[i]);
        }
    }  

    return students;    
}

showMenu();
let loop = true

while (loop) {
    let num = parseInt(readline.question("Введите команду:")); 
    let group, id, marks, mid, students, student;

    switch (num) {
        case 1:
            id = parseInt(readline.question("Введите номер зачетки:"));
            showStudent(id);
            break;
        case 2:
            group = readline.question("Введите группу:");
            id = parseInt(readline.question("Введите номер зачетки:"));
            marks = readline.question("Введите оценки:").split(" ");
            if (marks == "") {
                marks = [];
            }
            for (let i = 0; i < marks.length; i++) {
                marks[i] = parseInt(marks[i]);
            }

            addStudent(group, id, marks);
            break;
        case 3:
            id = parseInt(readline.question("Введите номер зачетки:"));
            removeStudent(id);
            break;
        case 4:
            id = parseInt(readline.question("Введите номер зачетки:"));
            group = readline.question("Введите группу:");
            marks = readline.question("Введите оценки:").split(" ");
            updateStudent(group, id, marks);
            break;
        case 5:
            id = parseInt(readline.question("Введите номер зачетки:"));
            mid = getMidMark(id);
            console.log("Средняя оценка: " + mid);
            break;
        case 6:
            students = [];
            group = readline.question("Введите группу:");
            students = studentsByGroup(group);
            for (let i = 0; i < students.length; i++) {
                console.log(students[i]["id"] + "\n" + "Оценки:");
                for (let j = 0; j < students[i]["marks"].length; j++) {
                    console.log(students[i]["marks"][j]);
                }
            } 
            break;
        case 7:
            group = readline.question("Введите группу:");
            student = studentWithMaxMarks(group);
            console.log(student["id"] + "\n" + "Оценки:");
            for (let j = 0; j < student["marks"].length; j++) {
                console.log(student["marks"][j]);
            }
            break;
        case 8:
            students = studentsNoMarks();
            for (let i = 0; i < students.length; i++) {
                console.log(students[i]["id"] + "\n" + "Оценки:");
                for (let j = 0; j < students[i]["marks"].length; j++) {
                    console.log(students[i]["marks"][j]);
                }
            } 
            break;
        case 9:
            showMenu();
            break;
        case 10:
            loop = false;
            break;
        default:
            console.log(num);
    }
}