"use strict";

var readline = require('readline-sync');

var storage = []

function showMenu() {
    console.log("Хранилище детей.\n\
1 - информация о ребенке\n\
2 - добавить ребенка\n\
3 - удалить ребенка\n\
4 - изменить параметры ребенка\n\
5 - показать средний возраст детей\n\
6 - показать самого старшего ребенка\n\
7 - показать информацию о детях, возраст которых входит в заданный интервал\n\
8 - показать информацию о детях, фамилия которых начинается с заданной буквы\n\
9 - показать информацию о детях, фамилия которых длиннее заданного количества символов\n\
10 - информацию о детях, фамилия которых начинается с гласной буквы\n\
11 - показать меню\n\
12 - выйти\
    ");
}

function addChild(surname, age) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"] == surname) {
            console.log("Ребенок с такой фамилией уже существует!");
            return;
        } 
    }

    let child = {};
    child["surname"] = surname;
    child["age"] = age;
    storage.push(child);
}

function showChild(surname) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"] == surname) {
            console.log(surname + " " + storage[i]["age"]);
            return;
        } 
    }

    console.log("Ребенка с такой фамилией не существует!");
}

function removeChild(surname) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"] == surname) {
            storage.splice(i, 1);
            return;
        }
    }

    console.log("Ребенка с такой фамилией не существует!");
}

function updateChild(surname, newAge) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"] == surname) {
            storage[i]["age"] = newAge;
            return;
        }
    }

    console.log("Ребенка с такой фамилией не существует!");
}

function getMidAge() {
    let sum = 0;

    if (storage.length === 0) {
        console.log("Массив пустой");
        return null;
    }

    for (let i = 0; i < storage.length; i++) {
        sum += storage[i]["age"];
    }  

    return sum / storage.length;
}

function getOldest() {
    let oldestChild = {};
    oldestChild["age"] = 0;

    if (storage.length == 0) {
        console.log("В хранилище детей нет");
        return null;
    }
    
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["age"] > oldestChild["age"]) {
            oldestChild["age"] = storage[i]["age"];
            oldestChild["surname"] = storage[i]["surname"];
        }
            
    }  

    return oldestChild;
}

function getInterval(downAge, upAge) {
    let children = [];

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["age"] >= downAge && storage[i]["age"] <= upAge) {
            children.push(storage[i]);
        }
    }  

    return children;
}

function startingAt(symb) {
    let children = [];
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"].charAt(0) == symb) {
            children.push(storage[i]);
        }
    }  

    return children;    
}

function longerThan(length) {
    let children = [];

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["surname"].length > length) {
            children.push(storage[i]);
        }
    }  

    return children;  
}

function startingWithVowel() {
    let children = [];
    let vowels = ['а','у','о','ы','и','э','ю','я','е','ё'];

    for (let i = 0; i < storage.length; i++) {
        let first = storage[i]["surname"].charAt(0);
        for (let j = 0; j < vowels.length; j++) {
            if (first == vowels[j]) {
                children.push(storage[i]);
                break;
            }
        }
    }  

    return children;
}

showMenu();
let loop = true

while (loop) {
    let num = parseInt(readline.question("Введите команду:")); 
    let surname, age, children;

    switch (num) {
        case 1:
            surname = readline.question("Введите фамилию ребенка:");
            showChild(surname);
            break;
        case 2:
            surname = readline.question("Введите фамилию ребенка:");
            age = parseInt(readline.question("Введите возраст ребенка:"));
            addChild(surname, age);
            break;
        case 3:
            surname = readline.question("Введите фамилию ребенка:");
            removeChild(surname);
            break;
        case 4:
            surname = readline.question("Введите фамилию ребенка:");
            age = parseInt(readline.question("Введите возраст ребенка:"));
            updateChild(surname, age);
            break;
        case 5:
            age = getMidAge();
            console.log("Средний возраст: " + age);
            break;
        case 6:
            let child = getOldest();
            if (child != null) {
                console.log(child["surname"] + " " + child["age"]);
            }
            break;
        case 7:
            let downAge = readline.question("Введите нижнюю границу интервала:");
            let upAge = readline.question("Введите верхнюю границу интервала:");
            children = getInterval(downAge, upAge);
            for (let i = 0; i < children.length; i++) {
                console.log(children[i]["surname"] + " " + children[i]["age"]);
            }
            break;
        case 8:
            let symb = readline.question("Введите букву:");
            children = startingAt(symb);
            for (let i = 0; i < children.length; i++) {
                console.log(children[i]["surname"] + " " + children[i]["age"]);
            }
            break;
        case 9:
            let length = readline.question("Введите кол-во символов:");
            children = longerThan(length);
            for (let i = 0; i < children.length; i++) {
                console.log(children[i]["surname"] + " " + children[i]["age"]);
            }
            break;
        case 10:
            children = startingWithVowel();
            for (let i = 0; i < children.length; i++) {
                console.log(children[i]["surname"] + " " + children[i]["age"]);
            }
            break;
        case 11:
            showMenu();
            break;
        case 12:
            loop = false;
            break;
        default:
            console.log(num);
    }
}