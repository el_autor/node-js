"use strict";

var readline = require('readline-sync');

var storage = []

function showMenu() {
    console.log("Хранилище детей.\n\
1 - показать точку\n\
2 - добавить точку\n\
3 - удалить точку\n\
4 - изменить точку\n\
5 - показать самые отдаленные точки\n\
6 - показать точки, находящиеся от заданной точки на расстоянии, не превышающем заданную константу\n\
7 - показать точки, находящиеся выше / ниже / правее / левее заданной оси координат\n\
8 - показать точки, входящие внутрь заданной прямоугольной зоны\n\
9 - показать меню\n\
10 - выйти\
    ");
}

function addPoint(name, x, y) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["name"] == name) {
            console.log("Точка с таким именем уже существует!");
            return;
        }
    }

    let point = {};
    point["name"] = name;
    point["x"] = x;
    point["y"] = y;
    storage.push(point);
}

function showPoint(name) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["name"] == name) {
            console.log(name + " " + storage[i]["x"] + " " + storage[i]["y"]);
            return;
        } 
    }

    console.log("Точки с таким именем не существует!");
}

function removePoint(name) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["name"] == name) {
            storage.splice(i, 1);
            return;
        }
    }

    console.log("Точки с таким именем не существует!");
}

function updatePoint(name, x, y) {
    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["name"] == name) {
            storage[i]["x"] = x;
            storage[i]["y"] = y;
            return;
        }
    }

    console.log("Точки с таким именем не существует!");
}

function getMaxDestination() {
    let dest = 0;

    for (let i = 0; i < storage.length; i++) {
        for (let j = 0; j < storage.length; j++) {
            if (i != j) {
                let localDest = Math.sqrt((storage[i]["x"] - storage[j]["x"]) * (storage[i]["x"] - storage[j]["x"]) + (storage[i]["y"] - storage[j]["y"]) * (storage[i]["y"] - storage[j]["y"]))
                if (localDest > dest) {
                    dest = localDest;
                }
            }
        }
    }  

    return dest;
}

function pointsInRadius(name, radius) {
    let point = null;
    let result = [];

    for (let i = 0; i < storage.length; i++) { 
        if (storage[i]["name"] == name) {
            point = storage[i];
        }
    }

    if (point == null) {
        console.log("Точка отсчета не найдена");
        return null;
    }

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["name"] != name) {
            let dest = Math.sqrt((point["x"] - storage[i]["x"]) * (point["x"] - storage[i]["x"]) + (point["y"] - storage[i]["y"]) * (point["y"] - storage[i]["y"]))
            if (dest <= radius) {
                result.push(storage[i]);
            }
        }
    }

    return result;
}

function decartPoints(axis, side) {
    let points = [];

    if (axis == "x") {
        if (side == "+") {
            for (let i = 0; i < storage.length; i++) {
                if (storage[i]["y"] >= 0) {
                    points.push(storage[i]);
                }
            }
        }
        else if (side == "-") {
            for (let i = 0; i < storage.length; i++) {
                if (storage[i]["y"] <= 0) {
                    points.push(storage[i]);
                }
            }   
        }
    }
    else if (axis == "y") {
        if (side == "+") {
            for (let i = 0; i < storage.length; i++) {
                if (storage[i]["x"] >= 0) {
                    points.push(storage[i]);
                }
            }
        }
        else if (side == "-") {
            for (let i = 0; i < storage.length; i++) {
                if (storage[i]["x"] <= 0) {
                    points.push(storage[i]);
                }
            }  
        }
    }

    return points;
}

function searchInRect(xUp, yUp, xDown, yDown) {
    let points = [];

    for (let i = 0; i < storage.length; i++) {
        if (storage[i]["x"] >= xUp && storage[i]["x"] <= xDown && storage[i]["y"] <= yUp && storage[i]["y"] >= yDown) {
            points.push(storage[i]);
        }
    }

    return points;    
}

showMenu();
let loop = true

while (loop) {
    let num = parseInt(readline.question("Введите команду:")); 
    let name, x, y, points, dest, axis, side;

    switch (num) {
        case 1:
            name = readline.question("Введите имя точки:");
            showPoint(name);
            break;
        case 2:
            name = readline.question("Введите имя точки:");
            x = parseInt(readline.question("Введите x:"));
            y = parseInt(readline.question("Введите y:"));
            addPoint(name, x, y);
            break;
        case 3:
            name = readline.question("Введите имя точки:");
            removePoint(name);
            break;
        case 4:
            name = readline.question("Введите имя точки:");
            x = parseInt(readline.question("Введите x:"));
            y = parseInt(readline.question("Введите y:"));
            updatePoint(name, x, y);
            break;
        case 5:
            dest = getMaxDestination();
            console.log("Максимальное расстояние: " + dest);
            break;
        case 6:
            name = readline.question("Введите имя точки:");
            dest = readline.question("Введите расстояние:");
            points = pointsInRadius(name, dest);
            for (let i = 0; i < points.length; i++) {
                console.log(name + " " + points[i]["x"] + " " + points[i]["y"]);
            }
            break;
        case 7:
            points = []
            axis = readline.question("Введите ось:");
            if (axis == "x") {
                side = readline.question("Введите сторону(+/-):");
                points = decartPoints(axis, side);
            }
            else if (axis == "y") {
                side = readline.question("Введите сторону(+/-):");
                points = decartPoints(axis, side);
            }

            for (let i = 0; i < points.length; i++) {
                console.log(points[i]["name"] + " " + points[i]["x"] + " " + points[i]["y"]);
            }
            break;
        case 8:
            let xUp = parseInt(readline.question("Левый верхний угол(x)"));
            let yUp = parseInt(readline.question("Левый верхний угол(y)"));
            let xDown = parseInt(readline.question("Правый нижний угол(x)"));
            let yDown = parseInt(readline.question("Правый верхний угол(y)"));
            points = searchInRect(xUp, yUp, xDown, yDown);
            for (let i = 0; i < points.length; i++) {
                console.log(points[i]["name"] + " " + points[i]["x"] + " " + points[i]["y"]);
            }
            break;
        case 9:
            showMenu();
            break;
        case 10:
            loop = false;
            break;
        default:
            console.log(num);
    }
}