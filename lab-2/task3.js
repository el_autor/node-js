"use strict";

let current = 0;

let firstInterval = setInterval(() => {
    current += 1;
    console.log(current);
    if (current === 10) {
        clearInterval(firstInterval);
    }
}, 2000);

setTimeout(() => {
    let secondInterval = setInterval(() => {
        current += 1;
        console.log(current);
        if (current === 20) {
            clearInterval(secondInterval);
        }
    }, 1000);
}, 20000);

let mainInterval = setInterval(() => {
    current = 0;

    let firstInterval = setInterval(() => {
        current += 1;
        console.log(current);
        if (current === 10) {
            clearInterval(firstInterval);
        }
    }, 2000);

    setTimeout(() => {
        let secondInterval = setInterval(() => {
            current += 1;
            console.log(current);
            if (current === 20) {
                clearInterval(secondInterval);
            }
        }, 1000);
    }, 20000);
}, 31000);