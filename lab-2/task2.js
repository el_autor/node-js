"use strict";

class Triangle {
    constructor(side1, side2, side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;

        this.eps = 0.0000001;
    }

    isValid() {
        if (this.side1 + this.side2 <= this.side3 ||
            this.side1 + this.side3 <= this.side2 ||
            this.side2 + this.side3 <= this.side1) {
            return false;
        } 

        return true;
    }   

    getPerimeter() {
        return this.side1 + this.side2 + this.side3;
    }

    getArea() {
        let halfP = this.getPerimeter() / 2;

        return Math.sqrt(halfP * (halfP - this.side1) * (halfP - this.side2) * (halfP - this.side3));
    }

    isRight() {
        let a = this.side1 * this.side1;
        let b = this.side2 * this.side2;
        let c = this.side3 * this.side3;

        if (Math.abs(a + b - c) <= this.eps ||
            Math.abs(a + c - b) <= this.eps ||
            Math.abs(b + c - a) <= this.eps) {
            return true;
        }

        return false;
    }
}

let t1 = new Triangle(3, 4, 5);
console.log(t1.getPerimeter());
console.log(t1.getArea());
console.log(t1.isValid());
console.log(t1.isRight());

console.log();

let t2 = new Triangle(4, 4, 5);
console.log(t2.getPerimeter());
console.log(t2.getArea());
console.log(t2.isValid());
console.log(t2.isRight());

console.log();

let t3 = new Triangle(3, 9, 5);
console.log(t3.isValid());