"use strict";

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    showInfo() {
        console.log("x:" + this.x + ", y:" + this.y);
    }
}

class Line {
    constructor(pointA, pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
    }

    showInfo() {
        console.log("Point A:");
        this.pointA.showInfo();
        console.log("Point B:");
        this.pointB.showInfo();
    }

    getLength() {
        return Math.sqrt((this.pointA.x - this.pointB.x) * (this.pointA.x - this.pointB.x) +
                         (this.pointA.y - this.pointB.y) * (this.pointA.y - this.pointB.y));
    }
}


let p1 = new Point(10, 10);
let p2 = new Point(20, 20);

p1.showInfo();
p2.showInfo();

let line1 = new Line(p1, p2);
line1.showInfo();
console.log(line1.getLength());