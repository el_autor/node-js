"use strict";

var readline = require('readline-sync');
var fs = require('fs');

class Node {

}

// создаю дерево из json объекта
// потом достигая максимальной глубины начинаю вывод с самого глубокого узла

let maxDepth = 0;
let maxDepthDouble = 0;
let outputDepth = 0;

function getDepth(rootNode, object, curDepth) {
    if (curDepth > maxDepth) {
        maxDepth = curDepth;
    }

    let entries = Object.entries(object);
    for (let i = 0; i < entries.length; i++) {
        if (Object.keys(entries[i][1]).length != 1) {
            getDepth(object, entries[i][1], curDepth + 1);
        }
    }
}

function showBranch(rootNode, object, curDepth) {
    if (curDepth > maxDepthDouble) {
        maxDepthDouble = curDepth;
        outputDepth = curDepth;
    }

    let entries = Object.entries(object);
    for (let i = 0; i < entries.length; i++) {
        if (Object.keys(entries[i][1]).length != 1) {
            showBranch(object, entries[i][1], curDepth + 1);
            //console.log(curDepth);
        }
    }

    if (maxDepthDouble == maxDepth) {
        if (curDepth == outputDepth && curDepth != 0) {
            console.log(Object.keys(rootNode));
            outputDepth -= 1;
        }
    }
}

let filename = readline.question("Введите имя файла:");
let JSONObject = JSON.parse(fs.readFileSync(filename, "utf8"));

getDepth(JSONObject, JSONObject, 0);
showBranch(JSONObject, JSONObject, 0);

console.log(maxDepth);

