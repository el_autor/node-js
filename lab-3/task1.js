"use strict";

var readline = require('readline-sync');
var fs = require('fs');

let array = []

let num = parseInt(readline.question("Введите число N:")); 

for (let i = 0; i < num; i++) {
    let string = readline.question();

    if (string.length % 2 == 0) {
        array.push(string);
    }
}

let resultString = JSON.stringify(array)
let filename = "task1.txt";

fs.writeFileSync(filename, resultString);