"use strict";

var str = "{ \"val\" : \"\"}";
var level = 1;
var object;

while (true) {
    object = JSON.parse(str);
    level += 1;
    //console.log(level);
    //console.log(str);

    try {
        str = str.replace("\"\"", "{ \"val\" : \"\"}");
        var result = JSON.stringify(object);
    }
    catch {
        console.log("Max = " + level);
        break;
    }
}
