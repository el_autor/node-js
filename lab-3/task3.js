"use strict";

var readline = require('readline-sync');
var fs = require('fs');

var extension = readline.question("Введите расширение:");
var path = readline.question("Введите путь до файла:");

let dir = fs.readdirSync(path);

for (let i = 0; i < dir.length; i++) {
    if (dir[i].endsWith(extension)) {
        console.log("Файл: " + dir[i]);
        console.log(fs.readFileSync(dir[i], "utf8"));
    }
}