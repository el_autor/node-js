"use strict";

var readline = require('readline-sync');
var fs = require('fs');

var n = readline.question("Введите N:");
var result = "";

for (let i = 0; i < n; i++) {
    let filename = readline.question();

    result += fs.readFileSync(filename, "utf8");
}

let newFilename = "task5.txt";

fs.writeFileSync(newFilename, result);