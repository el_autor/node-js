"use strict";

var readline = require('readline-sync');
var fs = require('fs');

var path = readline.question("Введите путь:");

function searchFiles(path) {
    if (fs.lstatSync(path).isDirectory()) {
        let dir = fs.readdirSync(path);
    
        for (let i = 0; i < dir.length; i++) {
            if (!fs.lstatSync(path + dir[i]).isDirectory()) {
                let fileData = fs.readFileSync(path + dir[i], "utf8");

                if (fileData.length <= 10) {
                    console.log(dir[i]);
                }                
                continue;
            }

            searchFiles(path + dir[i] + "/");
        }
    }
}

searchFiles(path);