"use strict";

let fs = require('fs');

let vowels = ['а','у','о','ы','и','э','ю','я','е','ё'];
let result = [];
let filename = "task2.txt";

let fileData = fs.readFileSync(filename, "utf8");

let rawData = JSON.parse(fileData);
let flag = true;

for (let i = 0; i < rawData.length; i++) {
    flag = true;
    let string = rawData[i];
    console.log(string);

    for (let j = 0; j < string.length; j++) {
        if (!(vowels.includes(string[j]))) {
            flag = false;
            break;
        }
    }

    if (flag) {
        result.push(string);
    }
}

console.log("Результат:");

for (let i = 0; i < result.length; i++) {
    console.log(result[i]);
}