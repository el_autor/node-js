"use strict";

let readline = require("readline-sync");
let fs = require("fs");

let paramsNumber = parseInt(readline.question("Введите количество параметров:"));
let adress = readline.question("Введите адрес:");

let htmlString = `<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Страница A</title>
</head>
<body>
    <h1>Страница A</h1>
    <form method="GET" action="`;

htmlString += adress + "\">";

for (let i = 0; i < paramsNumber; i++) {
    let paramName = readline.question("Введите название параметра:");

    htmlString += `
        <p>Введите ${paramName}</p>
        <input name="${paramName}" spellcheck="false" autocomplete="off">`;
}

htmlString += `
        <br>
        <br>
        <input type="submit" value="Отправить запрос">
    </form>
</body>`;

fs.writeFileSync("task3.html", htmlString);