"use strict";

const fs = require("fs");

const express = require("express");

const app = express();
const port = 5015;
app.listen(port);

app.get("/me/page", function(request, response) {
    const nameString = request.query.p;
    if (fs.existsSync(nameString)) {
        const contentString = fs.readFileSync(nameString, "utf8");
        response.end(contentString);
    } else {
        const contentString = fs.readFileSync("bad.html", "utf8");
        response.end(contentString);
    }
});

app.get("/sequence", function(request, response) {
    const a = parseInt(request.query.a);
    const b = parseInt(request.query.b);
    const c = parseInt(request.query.c);
    
    let resultString = "[";
    let first = true;

    for (let num = a; num <= b; num++) {
        if (num % c == 0) {
            if (first) {
                resultString += String(num);
                first = false;
            }
            else {
                resultString += "," + String(num);
            }
        } 
    }

    resultString += "]";
     
    const answerJSON = JSON.stringify({result: resultString});
    response.end(answerJSON);
});