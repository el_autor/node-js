"use strict";

const fs = require("fs");

const express = require("express");

const app = express();
const port = 5015;
app.listen(port);

app.get("/me/page", function(request, response) {
    const nameString = request.query.p;
    if (fs.existsSync(nameString)) {
        const contentString = fs.readFileSync(nameString, "utf8");
        response.end(contentString);
    } else {
        const contentString = fs.readFileSync("bad.html", "utf8");
        response.end(contentString);
    }
});

app.get("/getElemByIndex", function(request, response) {
    const index = request.query.index;
    const rawIndex = parseInt(index);
    
    const fileContents = fs.readFileSync("task2.txt", "utf8");
    const json = JSON.parse(fileContents)
     
    const answerJSON = JSON.stringify({result: json[rawIndex]});
    response.end(answerJSON);
});